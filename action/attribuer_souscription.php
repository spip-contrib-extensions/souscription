<?php

/**
 * Modification de l'objet souscription
 *
 * @plugin     Souscription
 * @copyright  2013
 * @author     Olivier T�tard
 * @licence    GNU/GPL
 * @package    SPIP\Souscription\Action
 */


if (!defined('_ECRIRE_INC_VERSION')) return;

function action_attribuer_souscription_dist($id_souscription=null, $id_auteur=null){

	if (is_null($id_souscription) or is_null($id_auteur)){
		$securiser_action = charger_fonction('securiser_action','inc');
		$arg = $securiser_action();

		list($id_souscription, $id_auteur) = explode('-', $arg);
	}

	if (autoriser('webmestre')) {

		if ($id_souscription = intval($id_souscription)
			AND $souscription = sql_fetsel("*","spip_souscriptions","id_souscription=".intval($id_souscription))
			AND $id_auteur = intval($id_auteur)
			AND $auteur = sql_fetsel("*","spip_auteurs","id_auteur=".intval($id_auteur))){

			if ($souscription['id_auteur'] == 0){
				sql_updateq('spip_souscriptions',array('id_auteur'=>$id_auteur),"id_souscription=" . intval($id_souscription));

				// et toutes les transactions li�es, sans auteur
				$id_transactions = sql_allfetsel('id_objet', 'spip_souscriptions_liens', "objet='transaction' AND id_souscription=" . intval($id_souscription));
				$id_transactions = array_column($id_transactions, 'id_objet');
				if (!empty($id_transactions)) {
					$id_transactions = sql_allfetsel('id_transaction', 'spip_transactions', "id_auteur=0 AND " . sql_in('id_transaction', $id_transactions));
					$id_transactions = array_column($id_transactions, 'id_transaction');
					if (!empty($id_transactions)) {
						sql_updateq('spip_transactions',array('id_auteur'=>$id_auteur),sql_in('id_transaction', $id_transactions));
					}
				}

			}

		}

	}
}