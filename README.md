# Souscription : dons et adhésions avec SPIP

## Présentation du *plugin*

Souscription est un *plugin* permettant de mettre en place, sur un
site fonctionnant sous [SPIP](http://www.spip.net/), des campagnes
d'adhésions ou de dons. Le paiement est délégué au *plugin* 
[Bank](https://github.com/nursit/bank>), qui prend en charge différentes
plateforme de paiement en ligne (chèque, PayBox, PayPal, virement,
etc.).

Ce *plugin* a été développé pour les besoins d'[Attac France](https://france.attac.org) 
et de [Basta!](https://basta.media)

## Installation

Comme tout plugin SPIP.

## Configuration

Il est possible de configurer le *plugin* (définition des montants
proposés pour l'adhésion et/ou le dons). Pour cela, il faut se rendre dans le menu « Configuration » puis « Souscriptions ».

## Première utilisation

La première étape pour utiliser le *plugin* souscription est de mettre
en place une campagne, d'adhésions ou de dons. Une campagne peut avoir
des objectifs et il est éventuellement possible de définir des
montants spécifiques.

Pour afficher le formulaire d'adhésion, il faut ensuite ajouter le
code suivant dans un article (en adaptant le numéro de la campagne) :

```html
<souscription|campagne=1>
```